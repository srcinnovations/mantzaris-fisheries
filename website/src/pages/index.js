import React from 'react';

import HTMLHeader from '../components/HTMLHeader';
import Layout from '../layouts/index';
import Splash from '../components/splash';
import Profile from '../components/profile';
import Quality from '../components/quality';
import Sourcing from '../components/sourcing';
import Products from '../components/products';
import Contact from '../components/contact';
import Footer from '../components/footer';
import '../layouts/index.scss';

const IndexPage = () => (
  <Layout isHomePage>
    <main>
      <Splash />
      <Profile />
      <Quality />
      <Sourcing />
      <Products />
      <Contact />
      <Footer />
    </main>
  </Layout>
);

/**
 * Call Gatsby Head API for metadata
 * @see https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/
 */
export const Head = () => <HTMLHeader />

export default IndexPage;
