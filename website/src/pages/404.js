import React from 'react';
import HTMLHeader from '../components/HTMLHeader';
import Layout from '../layouts/index';
import PageNotFound from '../components/page-not-found';

const NotFoundPage = () => (
  <Layout>
    <PageNotFound />
  </Layout>
);

/**
 * Call Gatsby Head API for metadata
 * @see https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/
 */
export const Head = () => <HTMLHeader />

export default NotFoundPage;
