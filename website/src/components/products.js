/* global window */
import React from 'react';
/* eslint-disable-next-line */
import './styles/products.scss';
import LeftArrow from '../svgs/left-arrow';

const _info = (index) => {
  switch(index) {
    case 'squid':
      return (
        <ul>
          <li>
            U5 frozen squid tubes, 5 Kg
          </li>
          <li>
            U10 frozen squid tubes, 5 Kg
          </li>
          <li>
            Pineapple cut squid, 5 Kg
          </li>
          <li>
            Frozen squid tentacles, 10 Kg
          </li>
          <li>
            Frozen squid wings, 10 Kg
          </li>
          <li>
            Frozen squid rings, 5 Kg
          </li>
          <li>
            Frozen squid ring, 5 x 1 Kg
          </li>
        </ul>
      );
    case 'scallops':
      return (
        <ul>
          <li>
            Frozen IQF scallops, roe on, 10 Kg
          </li>
          <li>
            Frozen IQF scallops, roe on, 9 x 1 Kg
          </li>
          <li>
            Half shell scallop, roe on, 9 dozen
          </li>
          <li>
            Vacuum sealed scallop trays, roe on, 6 x 500 g
          </li>
        </ul>
      );
    case 'fillets':
      return (
        <p>
          Frozen skin off Ling fillets/ Portion RW – 5 Kg (Australia and NZ)
        </p>
      );
    case 'reprocessing':
      return (
        <p>
          Mantzaris Fisheries also specializes in high volume fish processing
          from fresh and frozen raw material under customer’s brand name and
          assist in exporting on their behalf.
        </p>
      );
    default:
      return (
        <p>
          <pre>
            :(
          </pre>
        </p>
      );
  }
};

const products = [
  {
    src: '/images/squid.png',
    title: 'Arrow squid',
    info: _info('squid'),
  },
  {
    src: '/images/scallops.png',
    title: 'Scallops (Tasmania)',
    info: _info('scallops'),
  },
  {
    src: '/images/fillets.png',
    title: 'Ling',
    info: _info('fillets'),
  },
  {
    src: '/images/reprocessing.png',
    title: 'Reprocessing',
    info: _info('reprocessing'),
  },
];

class Products extends React.Component {

  state = {
    curIndex: 0,
    doTransition: false
  }

  slideLeft = () => {
    const { curIndex } = this.state;
    this.setState({
      doTransition: true,
    });

    window.setTimeout((() => { this.endTransition(curIndex+1) }).bind(this), 400);
  }

  slideRight = () => {
    const { curIndex } = this.state
    this.setState({
      doTransition: true,
    });

    window.setTimeout((() => { this.endTransition(curIndex-1) }).bind(this), 400);
  }

  endTransition = (n) => {
    this.setState({
      curIndex: n,
    });

    window.setTimeout(() => {
      this.setState({
        doTransition: false,
      })
    }, 400);
  }

  render() {
    const { curIndex } = this.state;

    return (
      <section id="products" className="products">
        <div className="container">
          <div className="row">
            <div className="col-md-10">
              <h2>Products</h2>
            </div>
          </div>
        </div>
        <div className="container-fluid">
          <div className="carousel">
            <div className="container">
              <div className="row">
                <div className="col-md-6 col-lg-4">
                  <div className="info-panel">


                    { curIndex > 0 &&
                      <div className="product image-00">
                        <img src={products[curIndex-1].src} alt={products[curIndex-1].title} />
                        <div className={this.state.doTransition ? 'blind change' : 'blind'} />
                        <div className={this.state.doTransition ? 'blind change' : 'blind'} />
                        <div className={this.state.doTransition ? 'blind change' : 'blind'} />
                        <div className={this.state.doTransition ? 'blind change' : 'blind'} />
                      </div>
                    }

                    <div className="product image-01">
                      <img src={products[curIndex].src} alt={products[curIndex].title} />
                      <div  className={this.state.doTransition ? 'blind change' : 'blind'} />
                      <div className={this.state.doTransition ? 'blind change' : 'blind'} />
                      <div className={this.state.doTransition ? 'blind change' : 'blind'} />
                      <div className={this.state.doTransition ? 'blind change' : 'blind'} />
                      { curIndex < products.length - 1 &&
                        <button className="leftArrow" onClick={this.slideLeft}>
                          <LeftArrow />
                        </button>
                      }

                      { curIndex > 0 &&
                        <button className="rightArrow" onClick={this.slideRight}>
                          <LeftArrow />
                        </button>
                      }
                    </div>

                    { products.length > curIndex + 1 &&
                      <div className="product image-02">
                        <img src={products[curIndex+1].src} alt={products[curIndex+1].title} />
                        <div  className={this.state.doTransition ? 'blind change' : 'blind'} />
                        <div className={this.state.doTransition ? 'blind change' : 'blind'} />
                        <div className={this.state.doTransition ? 'blind change' : 'blind'} />
                        <div className={this.state.doTransition ? 'blind change' : 'blind'} />
                      </div>
                    }

                    { products.length > curIndex + 2 &&
                      <div className="product image-03">
                        <img src={products[curIndex+2].src} alt={products[curIndex+2].title} />
                        <div  className={this.state.doTransition ? 'blind change' : 'blind'} />
                        <div className={this.state.doTransition ? 'blind change' : 'blind'} />
                        <div className={this.state.doTransition ? 'blind change' : 'blind'} />
                        <div className={this.state.doTransition ? 'blind change' : 'blind'} />
                      </div>
                    }

                    <div>
                      <h4>{products[curIndex].title}</h4>
                      {products[curIndex].info}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default Products;
