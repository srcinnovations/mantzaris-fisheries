import React from 'react';
import LogoFullLockupVertical from '../svgs/logo-full-lockup-vertical';

import '../layouts/index.scss';
import './styles/404.scss';


const PageNotFound = () => (
  <section id="404" className="page-not-found">
    <LogoFullLockupVertical className="logo" />
    <div>
      <h1>
        Page not found
      </h1>
      <p className="text-center">
        <a href="/" className="btn btn-light">
          Return to Home Page
        </a>
      </p>
    </div>
  </section>
)

export default PageNotFound;
