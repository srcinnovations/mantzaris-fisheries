/** -------------------------------------------------------------------------------------- */
/** HTMLHeader component:                                                                  */
/** Provides page metadata to pages via Gatsby Head API                                    */
/** @see https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/          */
/** -------------------------------------------------------------------------------------- */
import React from 'react';
import { withPrefix } from 'gatsby';
import config from './../layouts/config';

const HTMLHeader = () => (
  <>
    <meta charSet="UTF-8" />
    <title>{config.title}</title>
    <meta name="description" content={config.description} />
    <meta name="keywords" content={config.keywords}/>
    <meta name="msapplication-TileColor" content="#072B43" />
    <meta name="msapplication-TileImage" content={withPrefix('/icons/ms-icon-144x144.png')} />
    <meta name="theme-color" content="#072B43" />
    <meta name="og:type" content={config.title} />
    <meta name="og:description" content={config.description} />
    <meta name="og:url" content={config.canonicalUrl} />
    <meta name="og:image" content={config.image} />

    <link rel="canonical" href={config.canonicalUrl} />
    <link rel="apple-touch-icon" type="image/png" size="57x57" href={withPrefix('/icons/apple-icon-57x57.png')} />
    <link rel="apple-touch-icon" type="image/png" size="60x60" href={withPrefix('/icons/apple-icon-60x60.png')} />
    <link rel="apple-touch-icon" type="image/png" size="72x72" href={withPrefix('/icons/apple-icon-72x72.png')} />
    <link rel="apple-touch-icon" type="image/png" size="76x76" href={withPrefix('/icons/apple-icon-76x76.png')} />
    <link rel="apple-touch-icon" type="image/png" size="114x114" href={withPrefix('/icons/apple-icon-114x114.png')} />
    <link rel="apple-touch-icon" type="image/png" size="152x152" href={withPrefix('/icons/apple-icon-152x152.png')} />
    <link rel="apple-touch-icon" type="image/png" size="180x180" href={withPrefix('/icons/apple-icon-180x180.png')} />
    <link rel="apple-touch-icon" type="image/png" size="120x120" href={withPrefix('/icons/apple-icon-120x120.png')} />
    <link rel="icon" type="image/png" href={withPrefix('/icons/android-icon-192x192.png')} />
    <link rel="icon" type="image/png" href={withPrefix('/icons/android-icon-144x144.png')} />
    <link rel="icon" type="image/png" href={withPrefix('/icons/android-icon-96x96.png')} />
    <link rel="icon" type="image/png" href={withPrefix('/icons/android-icon-72x72.png')} />
    <link rel="icon" type="image/png" href={withPrefix('/icons/android-icon-48x48.png')} />
    <link rel="icon" type="image/png" href={withPrefix('/icons/android-icon-36x36.png')} />
    <link rel="shortcut icon" type="image/x-icon" href={withPrefix('/icons/favicon.ico')} />
    <link rel="icon" type="image/x-icon" href={withPrefix('/icons/favicon.ico')} />
    <script src={withPrefix('/scripts/mantzaris.js')}></script>
  </>
);

export default HTMLHeader;