import React from 'react';

import '../layouts/index.scss';
import './styles/quality.scss';

const Quality = () => (
  <section id="quality" className="quality">
    <div className="container">
      <div className="row">
        <div className="col-md-6">
          <h2>Quality</h2>
          <p className="lead">
            Mantzaris Fisheries is committed to providing our customers with
            products that are of the utmost quality.
          </p>
          <p>
            We ensure that all products are fit for their intended use, meet
            the requirements of FSANZ Food Standard Code, comply with the
            requirements of the Export Control (Fish and Fish Products) Rules 2021, the principles of HACCP and the requirements of the
            European Union.
          </p>
          <p>
            Our accreditations and certifications
          </p>
          <ul>
            <li>
              Registered for export with DAFF – Department of Agriculture, Fisheries and Forestry, European Union and Russian Federation
            </li>
            <li>
              SQF certified
            </li>
            <li>
              Prime Safe certified
            </li>
            <li>
              MSC certified
            </li>
            <li>
              Approved Coles supplier
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>
)

export default Quality;
