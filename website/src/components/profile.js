import React from 'react';

import '../layouts/index.scss';

const Profile = () => (
  <section id="profile" className="profile">
    <div className="container">
      <div className="row">
        <div className="col-md-10">
          <h2>Profile</h2>
          <p className="lead">
            Mantzaris Fisheries has been processing and distributing seafood
            products for over 30 years, with the Mantzaris Fisheries involvement
            in the seafood industry stretching over 45 years.
          </p>
        </div>
      </div>
      <div className="row">
        <div className="col-md-6">
          <h4>Supply</h4>
          <p>
            We provide a regular supply of seafood to local markets and we also
            export substantial quanitiies to Europe, Hong Kong and Canada.
          </p>
          <p>
            Our company is currently the largest producer/processor of arrow
            squid products in Australia and one the the top producers of
            scallops  for European markets and continue to be actively involved
            in the production and sale of fish fillets.
          </p>
          <p>
            During the peak season we employ between 50 and 60 people to
            guarantee raw stocks are promptly processed and production levels
            maintained to meet market demands.
          </p>
        </div>
        <div className="col-md-6">
          <h4>A family business</h4>
          <p>
            We are a large family owned and operated company. Steve, Peter and
            Sarah are actively involved with the day to day production, running
           of the company and managing export orders. The hands-on approach
           ensures we maintain the highest standards that are enjoyed by our
           customers.
          </p>
          <h4>Reprocessing</h4>
          <p>
            In addition to catering for local and export markets we also operate
            contract processing for other companies.
          </p>
        </div>
      </div>
    </div>
  </section>
)

export default Profile
