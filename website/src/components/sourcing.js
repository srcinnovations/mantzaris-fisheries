import React from 'react';

import './styles/sourcing.scss';

const Sourcing = () => (
  <section id="sourcing" className="sourcing">
    <div className="container">
      <div className="row">
        <div className="col-md-6">
          <h2>Sourcing</h2>
          <p className="lead">
            Mantzaris Fisheries source fresh and frozen raw material from
            Australia and New Zealand. We promote products that have been
            sourced sustainably or using approved fishing methods.
          </p>
          <p>
            Fresh raw material is sourced from the fishing vessels that operate
            under strict fishing regulations at both federal and state level in
            Australia.
          </p>
          <p>
            Ling is sourced only from MSC certified fisheries in New Zealand.
          </p>
          <p>
            All products are processed under the approved system that helps to
            trace directly back to their place of origin.
          </p>
          <p>
            We consciously act with integrity, generosity and stand by our
            family values, with the aim of delivering sustainable profit for
            our family, the community and the future environment.
          </p>
        </div>
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="sourcing-copy">
          </div>
        </div>
      </div>
    </div>
  </section>
)

export default Sourcing
