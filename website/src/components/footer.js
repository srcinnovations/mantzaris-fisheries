import React from 'react';
import LogoFullLockupVertical from '../svgs/logo-full-lockup-vertical';
import Instagram from '../svgs/instagram';
import Facebook from '../svgs/fb';
import Twitter from '../svgs/twitter';
/* eslint-disable-next-line */
import './styles/footer.scss';

const Footer = () => (
      <footer>
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <LogoFullLockupVertical />
            </div>
            <div className="col-md-4">
              <div className="social-links">
                <a href="#" className="social-link"><Instagram /></a>
                <a href="#" className="social-link"><Twitter /></a>
                <a href="#" className="social-link"><Facebook /></a>
              </div>
            </div>
            <div className="col-md-4">
              <p>
                ABN 26604641614 | ACN 131 882 525
              </p>
              <p>
                <small>(C) COPYRIGHT, 2018</small>
              </p>
            </div>
          </div>
        </div>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118895872-1"></script>
        <script dangerouslySetInnerHTML={{__html:`
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', 'UA-118895872-1');
         `
       }}>
        </script>
      </footer>
);

export default Footer;
