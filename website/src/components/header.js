/* global window, document */
import React, { useEffect, useState, useRef } from 'react';
import LogoFullLockupHorizontal from '../svgs/logo-full-lockup-horizontal';
import Logo from '../svgs/logo';
import Phone from '../svgs/phone';
import config from '../layouts/config';
/* eslint-disable-next-line */
import './styles/header.scss';



const Header = () => {
  const headerElement = useRef();
  const [zeroState, setZeroState] = useState(true);
  const [tap, setTap] = useState(false);
  const [isOpen, setIsOpen] = useState(false);

  const scrollSpy = () => {
    const headerHeight = headerElement.current.offsetHeight;
    const yPos = window.scrollY;
    if (yPos > headerHeight) {
      setZeroState(false);
    } else if (yPos <= headerHeight) {
      setZeroState(true);
    }
  }

  const handleTap = () => {
    setTap(true);
    setIsOpen(!isOpen);

    window.setTimeout(() => {
      setTap(false);
    }, 200);
  }

  const scrollTo = (e) => {
    e.preventDefault();
    const hash = e.target.hash;
    
    setIsOpen(false);

    window.Mantzaris.jump(hash, {
    duration: 200,
    callback: function() {
        // Adapted from:
        // https://www.nczonline.net/blog/2013/01/15/fixing-skip-to-content-links/
        var element = document.getElementById(hash.substring(1));

        if (element) {
            if (!/^(?:a|select|input|button|textarea)$/i.test(element.tagName)) {
                element.tabIndex = -1;
            }
            element.focus();
        }
      }
    });
  }

  /** On component mount bind event handler to scroll */
  useEffect(() => {
    window.addEventListener('scroll', scrollSpy);

    /** clean up even listeners */
    return () => {
      window.removeEventListener('scroll', scrollSpy);
    }
  }, []);

  return(
    <header className={zeroState ? '' : 'inverse'} ref={headerElement}>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div className={ 'nav-container ' + (tap ? 'tap' : '') + (isOpen ? 'is-open' : '') }>
              <LogoFullLockupHorizontal className="logo-full-lockup-horizontal" />
              <Logo className="logo" width="52.7" height="47.3" />
              <a className="call-us-link" href={'tel:' + config.telno.replace(/\s*/g, '')}>
                <Phone /> call us
              </a>
              <div>
                <p className="call-us-large" style={{ margin: 0 }}>
                  call us on {config.telno}
                </p>
                <button className="hamburger button" onClick={handleTap}>
                  <div className="bars">
                    <span></span>
                  </div>
                </button>
                <nav>
                  <ul className="nav">
                    <li className="nav-item">
                      <a className="nav-link active" href="#home" onClick={scrollTo}>Home</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href="#profile" onClick={scrollTo}>Profile</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href="#quality" onClick={scrollTo}>Quality</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href="#sourcing" onClick={scrollTo}>Sourcing</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" href="#contact" onClick={scrollTo}>Contact</a>
                    </li>
                  </ul>
                </nav>
            </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
