import React, { useEffect, useRef } from 'react';
import { Loader } from "@googlemaps/js-api-loader"
/* eslint-disable-next-line */
import './styles/contact.scss'

const loader = new Loader({
  apiKey: "AIzaSyAX3jLwShSWg7sigynrmguBSVzR40ov0cE",
  version: "weekly",
  libraries: ["maps", "marker"]
});

const Contact = () => {
  const mapElement = useRef();
  // const [maps, setMaps] = useState(undefined);
  const defaultMapOptions = {
    center: {
      lat: -38.1100648,
      lng: 144.3563433
    },
    zoom: 12,
    mapId: 'Mantzaris_Map'
  };

  useEffect(() => {
    let map;
    /** Generate Google Map */
    loader
      .importLibrary('maps')
      .then(({ Map }) => {
        map = new Map(mapElement.current, defaultMapOptions);
      });

    /** Add marker to Google Map */
    loader
      .importLibrary('marker')
      .then(({ AdvancedMarkerElement }) => {
        new AdvancedMarkerElement({
          map: map, // maps,
          position: {lat: -38.1100648, lng: 144.3563433},
          title: 'Mantzaris Fisheries',
        });
      });
  }, []);

  return (
    <section id="contact" className="contact">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h2>Contact</h2>
          </div>
          <div className="col-md-6">
            <h6>Factory address</h6>
            <p>44-54 Corio Quay Road,  North Geelong, Vic 3215, AUSTRALIA</p>
            <h6>Postal address</h6>
            <p>PO Box 1255,  Geelong, VIC 3220, AUSTRALIA</p>
          </div>
          <div className="col-md-6">
            <h6>Enquiries</h6>
            <p>
              <strong>Tel</strong> (03) 5277 1766&nbsp;&nbsp;|&nbsp;&nbsp;<strong>Fax</strong> (03) 5277 1767
            </p>
            <p>
              <strong>Email</strong> <a href="mailto:office@mantzarisfisheries.com.au">office@mantzarisfisheries.com.au</a>
            </p>
          </div>
        </div>
      </div>
      <div className="container-fluid">
        <div id="map" ref={mapElement}>
        </div>
      </div>
    </section>
  );
};

export default Contact;
