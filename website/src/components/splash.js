import React from 'react';

import DownArrow from '../svgs/down-arrow';

import '../layouts/index.scss';
import './styles/splash.scss';


const Splash = () => (
  <section id="home" className="splash">
    <div>
      <h1>
        Over 30 years experience <br />
        processing and distributing seafood <br />
        for Australian and international markets
      </h1>
      <a href="#profile" aria-label="Scroll to view website" className="animated pulse">
        <span>Scroll</span>
        <DownArrow className="down-arrow" />
      </a>
    </div>
  </section>
)

export default Splash
