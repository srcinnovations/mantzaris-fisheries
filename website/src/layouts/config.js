const config = {
  canonicalUrl: 'https://www.mantzarisfisheries.com.au',
  telno: '+613 5277 1766',
  title: 'Mantzaris Fisheries, Quality Seafood, Australia',
  description: 'Distributors and processors of seafood including Australian arrow squid, ling and scallops.  Specialists in import and export to all foreign markets.',
  keywords: 'seafood import, seafood export, arrow squid, scallops, ling, australian seafood',
  image: '/images/logo.png'
}

export default config
