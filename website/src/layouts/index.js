import React from 'react';
import PropTypes from 'prop-types';

import Header from '../components/header';

import './index.scss';

const Layout = ({ children, isHomePage = false }) => {
  return (
  <div className="TempHere">
      { isHomePage &&
        <Header />
      }
      {children}
  </div>
  )
};

Layout.propTypes = {
  children: PropTypes.object,
  isHomePage: PropTypes.bool
};

export default Layout;
