/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';

const LeftArrow = ({ className = '', width = '48', height = '48', colour = 'white', backgroundColour = '#E44813' }) => (
  <svg className={`${className}`.trim()} width={`${width}px`} height={`${height}px`}  x="0px" y="0px" focusable="false" viewBox="0 0 48 48">
    <g>
    	<rect fill={backgroundColour} width="48" height="48"/>
    </g>
    <path fill={colour} d="M31.7,33.6L22.2,24l9.5-9.6l-2.9-2.9L16.3,24l12.5,12.5L31.7,33.6z"/>

  </svg>
);

LeftArrow.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  className: PropTypes.string,
  colour: PropTypes.string,
};

export default LeftArrow;
