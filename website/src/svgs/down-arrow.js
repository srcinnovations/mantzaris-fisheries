/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';

const DownArrow = ({ className = '', width = '20', height = '77', colour = 'white' }) => (
  <svg className={`${className}`.trim()} width={`${width}px`} height={`${height}px`}  x="0px" y="0px" viewBox="0 0 19 75.9" focusable="false">
    <path fill={colour} d="M8.8,75.6c0.2,0.2,0.5,0.3,0.7,0.3s0.5-0.1,0.7-0.3l8.5-8.5c0.4-0.4,0.4-1,0-1.4s-1-0.4-1.4,0l-6.8,6.8V1c0-0.6-0.4-1-1-1s-1,0.4-1,1v71.4l-6.8-6.8c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4L8.8,75.6z"/>
  </svg>
);

DownArrow.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  className: PropTypes.string,
  colour: PropTypes.string,
};

export default DownArrow;
