/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';

const Logo = ({ className = '', width = '79.1', height = '70.9', colour = 'currentColor' }) => (
  <svg className={`${className}`.trim()} width={`${width}px`} height={`${height}px`}  x="0px" y="0px" viewBox="0 0 79.1 70.9" focusable="false">
    <g fill={colour}>
    	<g>
    		<path d="M59.4,59.2l-12.2,9.2c-1.8,1.2-1.5,2.3,0.7,2.3h5.6c2.2,0,4.8-0.3,6.4-1.4c1.3-0.8,2.4-6.6,2.4-8.8v-0.1C62.3,58.3,61,57.7,59.4,59.2z"/>
    		<path d="M30.9,68.5l-12.2-9.2c-1.6-1.5-2.9-1-2.9,1.2v0.1c0,2.2,1.1,7.9,2.4,8.8c1.6,1.1,4.2,1.4,6.4,1.4h5.6C32.4,70.7,32.7,69.7,30.9,68.5z"/>
    		<rect x="41.1" y="35.7" width="2.3" height="2.3"/>
    		<rect x="47.6" y="35.7" width="2.3" height="2.3"/>
    		<path d="M75.2,30.9c3.6-7.5-0.7-14.9-9.5-14.1c3-10.3-6.8-16.3-15.5-9.8C44.6-4.4,30.9-0.1,28.9,7.3c-8.1-6.8-18.2-0.7-15.7,9.5C3.5,16,0,24.5,3.7,30.9c-2.1,2.2-4.9,4.8-3.1,9.4c0.8,2.1,34.5,30.7,38.6,30.7c2.6,0,37.5-28.1,39.2-30.4C80.1,38.1,78.9,34.2,75.2,30.9z M57.2,20.2h1.7V40l-1.7,0.2V20.2z M53.3,32.3v8.4l-2.6,0.3c-2.6,0.3-5,1.1-7.3,2.3l0,0c-2.4,1.3-5.1,1.9-7.9,1.9h-1.7l1.4-10.1L33,32.3H53.3z M29.2,42.2V47h-7v-4.8H29.2z M17.1,27h-1.7v-4.3l11.1,5.9v12.7h-1.7V29.6l-7.8-4.2V27z M61.7,47.9c-0.4,1.7-1.9,2.9-3.7,2.9H20.5c-1,0-1.7-0.7-1.7-1.7v-2.8h2.6v1.7H30v-1.7h5.4c2.9,0,5.8-0.7,8.3-2.1l0,0c2.2-1.2,4.5-1.9,7-2.2l13-1.6L61.7,47.9z"/>
    	</g>
    </g>
  </svg>
);

Logo.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  className: PropTypes.string,
  colour: PropTypes.string,
};

export default Logo;
