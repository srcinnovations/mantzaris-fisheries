/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';

const Phone = ({ className = '', width = '24', height = '24', colour = 'currentColor' }) => (
  <svg className={`${className}`.trim()} width={`${width}px`} height={`${height}px`}  x="0px" y="0px" viewBox="0 0 24 24" focusable="false">
    <g>
      <path fill={colour} d="M20,15.5c-1.2,0-2.5-0.2-3.6-0.6c-0.4-0.1-0.7,0-1,0.2l-2.2,2.2c-2.8-1.4-5.2-3.8-6.6-6.6l2.2-2.2c0.3-0.3,0.4-0.7,0.2-1C8.7,6.4,8.5,5.2,8.5,4c0-0.5-0.4-1-1-1H4C3.5,3,3,3.5,3,4c0,9.4,7.6,17,17,17c0.5,0,1-0.5,1-1v-3.5C21,15.9,20.5,15.5,20,15.5zM19,12h2c0-5-4-9-9-9v2C15.9,5,19,8.1,19,12z M15,12h2c0-2.8-2.2-5-5-5v2C13.7,9,15,10.3,15,12z"/>
    </g>
  </svg>
);

Phone.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  className: PropTypes.string,
  colour: PropTypes.string,
};

export default Phone;
