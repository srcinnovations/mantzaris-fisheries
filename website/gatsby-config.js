module.exports = {
  siteMetadata: {
    title: 'Mantzris Fisheries, Quality Seafood, Australia',
    description: 'Distributors and processors of seafood including Australian arrow squid, ling and scallops.  Specialists in import and export to all foreign markets.',
    keywords: 'seafood import, seafood export, arros squid, scallops, ling',
  },
  plugins: [
  {
	resolve: 'gatsby-plugin-eslint',
	options: {
	  test: /\.js$|\.jsx$/,
	  exclude: /(node_modules|cache|public)/,
	  options: {
	    emitWarning: true,
	    failOnError: false
 	  }
    }
  },
  'gatsby-plugin-sass'
 ],
}
