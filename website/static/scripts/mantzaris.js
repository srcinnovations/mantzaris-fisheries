(function() {
  'use strict';

  window.Mantzaris = window.Mantzaris || {};
  var sections = [];

  function throttle(callback, limit) {
    var wait = false;
    return function () {
      if (!wait) {
        callback.call();
        wait = true;
        setTimeout(function () {
          wait = false;
        }, limit);
      }
    }
  }

  function fadeInOnScroll(e) {
    var bottom_of_window = window.scrollY + window.innerHeight;

    [].forEach.call(sections, function(el) {
      var bottom_of_object = el.offsetTop;
      /* If the object is completely visible in the window, fade it it */
      if( (bottom_of_window - window.innerHeight/4) > bottom_of_object ){

          el.classList.remove('hide');
          if (el.tagName === 'FOOTER') {
            window.onscroll = null;
          }
      }
    });
  }

  var Config = {
    GOOGLE_MAPS_API_KEY: 'AIzaSyAX3jLwShSWg7sigynrmguBSVzR40ov0cE',
    GOOGLE_TRACKING_ID: 'UA-118895872-1',
    LOCATION: {
      lat: -38.1100648,
      lng: 144.3563433
    },
    ZOOM: 12,
    ICON: '/icons/favicon-32x32.png',
    TITLE: 'Mantzaris Fisheries',
    MAP_ID: 'map'
  }

  window.Mantzaris.onload = function(fn) {
    if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
      fn();
    } else {
      document.addEventListener('DOMContentLoaded', fn);
    }
  }

  var initGTM = function() {
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', Config['GOOGLE_TRACKING_ID']);
  }

  window.Mantzaris.onload(function() {
    var head = document.getElementsByTagName('head')[0];

    // Add Google tag manager
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.onload = initGTM
    script.src = 'https://www.googletagmanager.com/gtag/js?id=' + Config['GOOGLE_TRACKING_ID'];
    script.setAttribute('async', true);
    head.appendChild(script);

    if (window.scrollY === 0) {
      sections = document.querySelectorAll('.profile, .quality, .sourcing, .products, .contact, footer');
      [].forEach.call(sections, function(el) {
        el.classList.add('hide')
      })

      window.onscroll = throttle(fadeInOnScroll, 100);
    }


  });

  window.Mantzaris.jump = function(n,o){var t,e,i=window.pageYOffset,a={duration:o.duration,offset:o.offset||0,callback:o.callback,easing:o.easing||function(n,o,t,e){return(n/=e/2)<1?t/2*n*n+o:-t/2*(--n*(n-2)-1)+o}},c="string"==typeof n?a.offset+document.querySelector(n).getBoundingClientRect().top:n,u="function"==typeof a.duration?a.duration(c):a.duration;function f(n){e=n-t,window.scrollTo(0,a.easing(e,i,c,u)),e<u?requestAnimationFrame(f):(window.scrollTo(0,i+c),"function"==typeof a.callback&&a.callback())}requestAnimationFrame(function(n){t=n,f(n)})}


})();
